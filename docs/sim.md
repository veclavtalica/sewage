# Simulation

## World
Grid composed of sums of blocks, things and specks

----------------------------
## Block
Position: Grid regular

---------------------------
## Thing
Position: Grid regular, with offset

Allowed to have property fields

---------------------------
## Speck
Position: Grid regular

Allows multiple stratas of different materials

---------------------------
## Material
Describes qualities

By behavior: powder, fluid, gas
