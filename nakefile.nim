# Copyright 2017 Xored Software, Inc.

import nake
import os, times
import godotapigen

proc genGodotApi() =
  let godotBin = getEnv("GODOT_BIN")
  if godotBin.len == 0:
    echo "GODOT_BIN environment variable is not set"
    quit(-1)
  if not fileExists(godotBin):
    echo "Invalid GODOT_BIN path: " & godotBin
    quit(-1)

  const targetDir = "src"/"godotapi"
  createDir(targetDir)
  const jsonFile = targetDir/"api.json"
  if not fileExists(jsonFile) or
     godotBin.getLastModificationTime() > jsonFile.getLastModificationTime():
    direShell(godotBin, "--gdnative-generate-json-api", getCurrentDir()/jsonFile)
    if not fileExists(jsonFile):
      echo "Failed to generate api.json"
      quit(-1)

    genApi(targetDir, jsonFile)

task "build", "Builds the client for the current platform":
  genGodotApi()
  let bitsPostfix = when sizeof(int) == 8: "64" else: "32"
  let libFile =
    when defined(windows):
      "win" & bitsPostfix & ".dll"
    elif defined(ios):
      "ios" & bitsPostfix & ".dylib"
    elif defined(macosx):
      "mac.dylib"
    elif defined(android):
      "libandroid.so"
    elif defined(linux):
      "linux" & bitsPostfix & ".so"
    else: nil
  direShell(["nim", "c", "-o:addons/core"/libFile, "src"/"core.nim"])

task "build-release", "Builds the client for the current platform in release":
  genGodotApi()
  let bitsPostfix = when sizeof(int) == 8: "64" else: "32"
  let libFile =
    when defined(windows):
      "win" & bitsPostfix & ".dll"
    elif defined(ios):
      "ios" & bitsPostfix & ".dylib"
    elif defined(macosx):
      "mac.dylib"
    elif defined(android):
      "libandroid.so"
    elif defined(linux):
      "linux" & bitsPostfix & ".so"
    else: nil
  direShell(["nim", "c", "-o:addons/core"/libFile, "-d:release", "--opt:speed", "src"/"core.nim"])

task "clean", "Remove files produced by build":
  removeDir(".nimcache")
  removeDir("src"/".nimcache")
  removeDir("src"/"godotapi")
  removeFile("nakefile")
  removeFile("nakefile.exe")
