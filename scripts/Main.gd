extends Node

const CAMERA_SPEED := 16
const DEBUG_UPDATE_PER_TICKS := 60

var debug_update_counter := 0

var brush_active := true
var current_block_brush := 0

var freecam_active := true
var freecam_position := Vector2.ZERO
var freecam_inputs := 0

onready var n_WorldServer = find_node("WorldServer")
onready var n_WorldCanvas = n_WorldServer.find_node("WorldCanvas")
onready var n_DebugPanel = find_node("DebugPanel")
onready var n_Minimap = n_DebugPanel.find_node("Minimap")

var _is_debug := false

func _ready() -> void:
  if OS.has_feature("debug"):
    _is_debug = true
    n_DebugPanel.show()
  else:
    OS.low_processor_usage_mode = true
    set_process(false)

func _process(d: float) -> void:
  if freecam_active:
    _process_camera(d)
  if _is_debug:
    debug_update_counter += 1
    if debug_update_counter == DEBUG_UPDATE_PER_TICKS:
      n_Minimap.reflect(n_WorldServer, n_WorldCanvas)
      debug_update_counter = 0

func _input(i: InputEvent) -> void:
  if freecam_active:
    if i is InputEventKey:
      match i.scancode:
        KEY_D:
          if i.pressed: freecam_inputs = freecam_inputs | 0b0001
          else: freecam_inputs = freecam_inputs & ~0b0001
        KEY_A:
          if i.pressed: freecam_inputs = freecam_inputs | 0b0010
          else: freecam_inputs = freecam_inputs & ~0b0010
        KEY_S:
          if i.pressed: freecam_inputs = freecam_inputs | 0b0100
          else: freecam_inputs = freecam_inputs & ~0b0100
        KEY_W:
          if i.pressed: freecam_inputs = freecam_inputs | 0b1000
          else: freecam_inputs = freecam_inputs & ~0b1000

  if _is_debug and brush_active:
    if i is InputEventMouse:
      if i.button_mask == BUTTON_LEFT:
        n_WorldServer.set_block(to_world_coords(i.position), current_block_brush)
      if i.button_mask == BUTTON_RIGHT:
        n_WorldServer.set_void(to_world_coords(i.position))

func to_world_coords(p: Vector2) -> Vector2:
  var camera = n_WorldCanvas.get_camera_position()
  var tile_dims = n_WorldCanvas.get_tile_dimensions()
  var d = p / tile_dims + camera
  return Vector2(floor(d.x), floor(d.y))

func _process_camera(d: float) -> void:
  if freecam_inputs & 0b0001:
    freecam_position.x += CAMERA_SPEED * d
  if freecam_inputs & 0b0010:
    freecam_position.x -= CAMERA_SPEED * d
  if freecam_inputs & 0b0100:
    freecam_position.y += CAMERA_SPEED * d
  if freecam_inputs & 0b1000:
    freecam_position.y -= CAMERA_SPEED * d
  n_WorldCanvas.set_camera_position(freecam_position)
