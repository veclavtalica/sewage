extends Node

# todo: Zooming
# todo: Scrolling

var show_cell_colors := true
var show_camera_rect := true

onready var n_Texture = find_node("Texture")

func _ready() -> void:
  if $ShowColors.connect("toggled", self, "_on_show_colors_toggled") != OK:
    assert(false)
  if $ShowCamera.connect("toggled", self, "_on_show_camera_toggled") != OK:
    assert(false)

func reflect(w: WorldServer, c: WorldCanvas) -> void:
  var dims = w.get_world_dimensions()
  var img = Image.new()
  img.create(dims.x, dims.y, true, Image.FORMAT_RGB8)
  img.fill(Color.black)
  img.lock()
  if show_cell_colors:
    for x in dims.x:
      for y in dims.y:
        img.set_pixel(x, y, w.get_cell_color(Vector2(x, y)))
  if show_camera_rect:
    var canvas_dims = c.get_canvas_dimensions()
    var cam = c.get_camera_position()
    for x in canvas_dims.x:
      img.set_pixel(int(cam.x) + x, int(cam.y), Color.white)
      img.set_pixel(int(cam.x) + x, int(cam.y) + int(canvas_dims.y), Color.white)
    for y in canvas_dims.y:
      img.set_pixel(int(cam.x), int(cam.y) + y, Color.white)
      img.set_pixel(int(cam.x) + int(canvas_dims.x), int(cam.y) + y, Color.white)
  img.unlock()
  var tex = ImageTexture.new()
  tex.create_from_image(img)
  n_Texture.texture = tex

func _on_show_colors_toggled(a: bool) -> void:
  show_cell_colors = a

func _on_show_camera_toggled(a: bool) -> void:
  show_camera_rect = a
