import color
export color

type
  Material* = distinct uint8
    ## Zero material is reserved to denote absence

  SpeckBehavior* = enum
    sbFluid
      ## Distributes downwards and sideways

  MaterialDescriptor = object
    name: string
    color: ColorIdx
    desc: string
    speck_behavior: SpeckBehavior

const registry = [
  MaterialDescriptor.default,
  MaterialDescriptor(
    name: "sludge",
    color: cnGreen,
    desc: ":)"
  ),
]

# todo: Use pure enums
const
  mkNuffin* = 0.Material
  mkSludge* = 1.Material

# todo: For modding and dynamically generating content
var dyn_registry: seq[MaterialDescriptor]

# todo: Should be able to access separate runtime editable vector of descriptors
# todo: For static Material access we can precompute the address to get dynamic vector
template implGetter(name: untyped): untyped =
  proc `name`*(a: static[Material]): MaterialDescriptor.`name` {.inject, inline.} =
    static: doAssert a.uint64 < registry.len.uint64
    registry[a.uint64].`name`

  proc `name`*(a: Material): MaterialDescriptor.`name` {.inject, inline.} =
    assert a.uint64 < registry.len.uint64
    registry[a.uint64].`name`

implGetter name
implGetter color
implGetter desc
implGetter speck_behavior

converter toUint8*(a: Material): uint8 = a.uint8
