import std/macros
import ./vec
export vec

type
  Matrix*[T] = object
    ## Column-major
    data*: seq[T]
    dims*: IVec

  Direction* = enum
    dirNone
    dirUp         = 0b0001
    dirRight      = 0b0010
    dirUpRight    = 0b0011
    dirDown       = 0b0100
    dirDownRight  = 0b0110
    dirLeft       = 0b1000
    dirUpLeft     = 0b1001
    dirDownLeft   = 0b1100

{.push inline.}

func newMatrix*[T](dims: IVec): Matrix[T] =
  assert dims >= 0
  result.data = newSeq[T](dims.area)
  result.dims = dims

func toIdx[T](a: Matrix[T], p: IVec): int32 =
  assert p in a.dims
  p.y + p.x * a.dims.height

func `[]`*[T](a: Matrix[T], p: IVec): T =
  a.data[a.toIdx p]

func `[]`*[T](a: var Matrix[T], p: IVec): var T =
  a.data[a.toIdx p]

func `[]=`*[T](a: var Matrix[T], p: IVec, v: sink T) =
  a.data[a.toIdx p] = v

func contains*[T](a: Matrix[T], p: IVec): bool =
  p in a.dims

iterator items*[T](a: Matrix[T]): tuple[x, y: int32, v: T] =
  for (x, y) in a.dims:
    yield (x, y, a.data[a.toIdx (x, y)])

iterator mutFromBottomInDirection*[T](a: var Matrix[T], dir: int32): tuple[x, y: int32, v: var T] =
  # todo: It might be better to define direction by enum
  if dir > 0:
    for x in countup(0i32, a.dims.width - 1, dir):
      for y in countdown(a.dims.height - 1, 0):
        yield (x, y, a.data[a.toIdx (x, y)])
  else:
    for x in countdown(a.dims.width - 1, 0, abs(dir)):
      for y in countdown(a.dims.height - 1, 0):
        yield (x, y, a.data[a.toIdx (x, y)])

iterator rect*(a: typedesc[Direction]): Direction =
  yield dirUp
  yield dirUpRight
  yield dirRight
  yield dirDownRight
  yield dirDown
  yield dirDownLeft
  yield dirLeft
  yield dirUpLeft

iterator cross*(a: typedesc[Direction]): Direction =
  yield dirUp
  yield dirRight
  yield dirDown
  yield dirLeft

iterator rectDirs*(a: typedesc[Direction]): tuple[dir: Direction, diff: IVec] =
  yield (dirUp,         (+0i32, -1i32))
  yield (dirUpRight,    (+1i32, -1i32))
  yield (dirRight,      (+1i32, +0i32))
  yield (dirDownRight,  (+1i32, +1i32))
  yield (dirDown,       (+0i32, +1i32))
  yield (dirDownLeft,   (-1i32, +1i32))
  yield (dirLeft,       (-1i32, +0i32))
  yield (dirUpLeft,     (-1i32, -1i32))

iterator crossDirs*(a: typedesc[Direction]): tuple[dir: Direction, diff: IVec] =
  yield (dirUp,         (+0i32, -1i32))
  yield (dirRight,      (+1i32, +0i32))
  yield (dirDown,       (+0i32, +1i32))
  yield (dirLeft,       (-1i32, +0i32))

iterator neighboursRect*[T](a: Matrix[T], p: IVec): tuple[dir: Direction, v: T] =
  for (dir, diff) in Direction.rectDirs:
    yield (dir, a[p + diff])

iterator neighboursCross*[T](a: Matrix[T], p: IVec): tuple[dir: Direction, v: T] =
  for (dir, diff) in Direction.crossDirs:
    yield (dir, a[p + diff])

{.pop.}
