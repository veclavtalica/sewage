import std/[math, enumerate]

import pkg/godot
import ../godotapi/[scene_tree, visual_server, resource_loader, node, canvas_item]

import worldserver
import ../world, ../vec, ../rect, ../matrix

const
  # todo: Move to WorldCanvas fields and make them settable
  tile_dims = vec2(32, 32)
  texture_tile_dims = vec2(24, 24)

gdobj WorldCanvas of Node2D:
  ## Should always be children of WorldServer
  var server: WorldServer
  var camera_position*: Vector2
  var canvas_item: RID
  var canvas_dims: IVec
  var texture: Texture
  var texture_rid: RID
  var needs_redrawing: bool

  method init* =
    self.setProcess(true)
    self.texture = load("res://content/page.png") as Texture
    doAssert self.texture != nil
    self.texture_rid = initRID(godotObject self.texture)
    self.canvas_item = canvasItemCreate()
    canvasItemSetParent(self.canvas_item, self.getCanvasItem())

  method ready* =
    # todo: We might need to walk the scene tree until server is found, or display error
    self.server = self.getParent() as WorldServer
    self.resizeCanvas()
    doAssert self.getViewport().connect("size_changed", self, "_resize_canvas") == Error.Ok

  method process*(d: float32) =
    # todo: Shouldn't be needed every frame
    #       Make it happen when input was received or viewed chunks are processed
    if self.needs_redrawing or self.server.needs_redrawing:
      self.update()
      self.needs_redrawing = false
      self.server.needs_redrawing = false

  method draw* =
    self.update_canvas()

  method setCameraPosition*(pos: Vector2) {.gdExport.} =
    let rect = (pos.fromGodot, self.canvas_dims)
    let world = self.server.getWorld()
    if rect notin world.matrix.dims:
      # todo: Fit doesnt work correctly
      self.camera_position = rect.fit(world.matrix.dims).pos
    else:
      self.camera_position = pos
    self.needs_redrawing = true

  method getCameraPosition*: Vector2 {.gdExport.} =
    self.camera_position

  method getTileDimensions*: Vector2 {.gdExport.} =
    tile_dims

  method getCanvasDimensions*: Vector2 {.gdExport.} =
    self.canvas_dims

  method resizeCanvas* =
    let dims = self.getViewportRect().size / tile_dims
    self.canvas_dims = dims + 2

  method updateCanvas* =
    let
      world = self.server.getWorld()
      item = self.canvas_item
      cam: IVec = self.camera_position.floor()
      dx = (self.camera_position.x - self.camera_position.x.trunc()) * tile_dims.x.float32
      dy = (self.camera_position.y - self.camera_position.y.trunc()) * tile_dims.y.float32
    var
      tile_rect = initRect2(Vector2.default, tile_dims)
      texture_rect = initRect2(Vector2.default, texture_tile_dims)

    canvasItemClear(item)

    template renderBlock {.dirty.} =
      tile_rect.position = vec2(x.float32 * tile_dims.x - dx, y.float32 * tile_dims.y - dy)
      var mask = 0u32
      for (dir, next) in world.matrix.neighboursCross((wx, wy)):
        # todo: We can compare the memory of cellsums here directly, as every bit is significant
        if next.kind == ckBlock and next.block_v == cell.block_v:
          mask = mask or dir.uint32
      texture_rect.position = vec2(mask.float32 * texture_tile_dims.x, 0)
      item.canvasItemAddTextureRectRegion(
        tile_rect,
        self.texture_rid,
        texture_rect,
        modulate = cell.block_v.color
      )

    template renderSpeck {.dirty.} =
      var mat = cell.speck_v[7]
      var count: float32
      for n, i in enumerate countdown(6, -1):
        if i == -1 or cell.speck_v[i] != mat:
          if mat != mkNuffin:
            let rx = x.float32 * tile_dims.x - dx
            let ry = y.float32 * tile_dims.y + (n.float32 - count) * (tile_dims.y / 8) - dy
            item.canvasItemAddRect(
              initRect2(vec2(rx, ry), vec2(tile_dims.x, (count + 1) * tile_dims.y / 8)),
              mat.color
            )
          if i != -1:
            mat = cell.speck_v[i]
            count = float32.default
        else:
          count += 1

    for (x, y) in self.canvas_dims:
      let wx = x + cam.x
      let wy = y + cam.y
      let cell = world.matrix[(wx, wy)]
      case cell.kind:
      of ckBlock: renderBlock()
      of ckSpeck: renderSpeck()
      else: discard
