import pkg/godot
import ../godotapi/[node, scene_tree]
import ../world, ../matrix

# todo: Investigate whether Vector2i is exposed in GDNative, but not implemented in godot-nim

gdobj WorldServer of Node:
  # todo: Expose fields via generating variant converters
  # todo: Release RIDs
  var world: world.World
  var seed: uint64
  var needs_redrawing*: bool
    ## Signals to WorldCanvas that state is meaningfully changed

  method ready* =
    ## Note: Currently seed should be set before `ready` is invoked
    ## We might consider require calling `generate` explicitly, which would allow reuse
    self.world = initWorld((128i32, 128i32), self.seed)

  method physicsProcess*(delta: float32) =
    # todo: Should only need redrawing when there are active chunks
    self.needs_redrawing = true
    self.world.step()

  method getWorldDimensions*: Vector2 {.gdExport.} =
    self.world.matrix.dims

  method setBlock*(pos: Vector2, id: uint64) {.gdExport.} =
    if not (pos.inRectOf self.world.matrix.dims) and pos in self.world.matrix.dims:
      self.world.matrix[pos] = CellSum(kind: ckBlock, block_v: id.Block)

  method setVoid*(pos: Vector2) {.gdExport.} =
    if not (pos.inRectOf self.world.matrix.dims) and pos in self.world.matrix.dims:
      self.world.matrix[pos] = initVoidCellSum()

  method setSeed*(seed: uint64) {.gdExport.} =
    self.seed = seed

  method getCellType*(pos: Vector2): uint64 {.gdExport.} =
    let cell = self.world.matrix[pos]
    cell.kind.uint64

  method getCellName*(pos: Vector2): string {.gdExport.} =
    let cell = self.world.matrix[pos]
    case cell.kind:
    of ckBlock:
      cell.block_v.name
    else: "mystery"

  method getCellColor*(pos: Vector2): Color {.gdExport.} =
    let cell = self.world.matrix[pos]
    case cell.kind:
    of ckBlock:
      cell.block_v.color
    of ckEdge:
      cnBlack
    of ckSpeck:
      # todo: Would be better to reflect the majority
      cell.speck_v[0].color
    else: cnOpaque

  func getWorld*: ptr world.World = addr self.world
