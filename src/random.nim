import std/times

type
  Generator* = object
    state: uint64

func initGenerator*(seed: uint64): Generator =
  Generator(state: seed)

proc initGenerator*: Generator =
  Generator(state: getTime().nanosecond.uint64)

func next*(a: var Generator): uint64 =
  var x = a.state
  x = x xor (x shl 13)
  x = x xor (x shr 17)
  x = x xor 5
  a.state = x
  x

func get*(a: var Generator, T: typedesc[Ordinal]): T {.inline.} =
  when T is bool:
    a.next() < static (uint64.high div (bool.high.uint64 + 1))
  else:
    cast[T](a.next() mod (T.high.uint64 + 1) + T.low.uint64)
