import vec, matrix, random, blocks, materials
export vec, blocks, materials

type
  CellKind* {.size: sizeof(uint8).} = enum
    ckSpeck ## Can represent emptiness
    ckEdge ## Represents the end of the world
    ckBlock
    ckThing

  CellSum* = object
    case kind*: CellKind:
    of ckSpeck: speck_v*: Speck
    of ckBlock: block_v*: Block
    of ckThing: thing_v*: Thing
    of ckEdge: discard

  World* = object
    matrix*: Matrix[CellSum]

  Thing* = object
    ty*: ThingTemplate
      ## Template identifier
    ofs*: tuple[x: uint8, y: uint8]
      ## Offset from the grid cell top-left corner
    id*: uint16
      ## Id to access chunk specific property registry

  Speck* = array[8, Material]
    ## Up to 8 layers of materials, starting from bottom
    ## Index 0 is at the bottom, while 7 is at the top
    ## Assumption is that handling specks at the bottom is more common,
    ## for which accessing lower portion of the world will be faster as you can use 8 bit instructions for registers

  ThingTemplate* = object
    id*: uint16
    is_complex* {.bitsize:1.}: bool
      ## Specifies whether it consists of multiple parts

const
  # todo: Do we need to segregate world into chunks even? At least, for now it doesnt seem to be advantageous
  ChunkSize = 32
    ## Extend in each dimension for one chunk

func initSpeckWithTop*(m: Material): Speck = [mkNuffin, mkNuffin, mkNuffin, mkNuffin, mkNuffin, mkNuffin, mkNuffin, m]
func initSpeckFilled*(m: Material): Speck = [m, m, m, m, m, m, m, m]
func initVoidCellSum*: CellSum = CellSum(kind: ckSpeck, speck_v: static initSpeckWithTop(mkNuffin))

func initWorld*(size: IVec, seed: uint64): World =
  assert size mod ChunkSize == 0
  result.matrix = newMatrix[CellSum](size)
  var g = initGenerator(seed)
  for p in size:
    if g.get(bool):
      result.matrix[p] = CellSum(kind: ckBlock, block_v: bkBullshit)
    else:
      result.matrix[p] = CellSum(kind: ckSpeck, speck_v: static initSpeckFilled(mkSludge))
  for p in size.rect:
    result.matrix[p] = CellSum(kind: ckEdge)

proc step*(w: var World) =
  ## All entities are stepping towards reaching equilibrium, after which they are leaving the simulation
  ## Specks are update in `waves`, its done so that distribution will not be biased, and also so that each iteration could be discrete
  var wave {.global.}: int32 = 1
  # todo: Ignore stepping over edges via iterating over IRect
  # todo: Split
  for (x, y, cur) in w.matrix.mutFromBottomInDirection(wave):
    case cur.kind:
    of ckSpeck:
      for i, m in cur.speck_v:
        case m.speck_behavior:
        of sbFluid:
          if i == 0:
            # todo: It might be better to get mutable pointer to adjacent cell, as we're intending to write and access it
            let adjacent = w.matrix[(x, y + 1)]
            if adjacent.kind == ckSpeck and adjacent.speck_v[7] == mkNuffin:
              w.matrix[(x, y + 1)].speck_v[7] = m
              cur.speck_v[0] = mkNuffin
            elif adjacent.kind == ckSpeck and adjacent.speck_v[7] == m:
              let adjacent = w.matrix[(x - wave, y + 1)]
              if adjacent.kind == ckSpeck and adjacent.speck_v[7] == mkNuffin:
                w.matrix[(x - wave, y + 1)].speck_v[7] = m
                cur.speck_v[0] = mkNuffin
          else:
            if cur.speck_v[i - 1] == mkNuffin:
              cur.speck_v[i - 1] = m
              cur.speck_v[i] = mkNuffin
            else:
              let adjacent = w.matrix[(x - wave, y)]
              if adjacent.kind == ckSpeck and adjacent.speck_v[i - 1] == mkNuffin:
                w.matrix[(x - wave, y)].speck_v[i - 1] = m
                cur.speck_v[i] = mkNuffin
        else: discard
    else: discard
  wave = if wave == 1: -1 else: 1

assert sizeof(CellSum) == sizeof(uint64) * 2

converter toUint64*(a: Block): uint64 = a.uint64
